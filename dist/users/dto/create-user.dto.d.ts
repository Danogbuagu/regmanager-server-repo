export declare class CreateUserDto {
    readonly username: string;
    readonly email: string;
    readonly sname: string;
    readonly fname: string;
    readonly onames: string;
    readonly phoneNo: string;
    readonly roles: string[];
    readonly active: boolean;
    password: string;
}
