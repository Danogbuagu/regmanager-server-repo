"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const mongoose_plugin_autoinc_1 = require("mongoose-plugin-autoinc");
exports.UserSchema = new mongoose.Schema({
    username: { type: String, unique: true, required: true },
    email: { type: String, required: true, unique: true, lowercase: true },
    sname: { type: String, required: true },
    fname: { type: String, required: true },
    onames: { type: String, default: '' },
    phoneNo: { type: String, default: '' },
    roles: { type: [''], required: true, default: ['user'] },
    active: { type: Boolean, default: true },
    password: { type: String, default: '' },
    passwordHash: { type: String, default: '' },
}, {
    timestamps: true,
});
exports.UserSchema.plugin(mongoose_plugin_autoinc_1.autoIncrement, {
    model: 'User',
    field: 'userId',
    startAt: 1000,
    incrementBy: 1,
});
//# sourceMappingURL=user.schema.js.map