import { CreateUserDto } from './dto/create-user.dto';
import { User } from './user.interface';
import { Model } from 'mongoose';
export declare class UsersService {
    private readonly userModel;
    private saltRounds;
    constructor(userModel: Model<User>);
    getHash(password: string | undefined): Promise<string>;
    compareHash(password: string | undefined, hash: string | undefined): Promise<boolean>;
    createUser(userDto: CreateUserDto): Promise<User>;
    findAll(): Promise<User[]>;
    findOne(phoneNo: string): Promise<User>;
    getUserByUsername(username: string): Promise<User>;
    update(id: string): Promise<User>;
    deleteOne(id: string): Promise<User>;
    findOneByToken(token: string): Promise<User>;
    getUserByEmail(email: string): Promise<User>;
    findOneByEmail(email: any): Promise<User>;
}
