export declare class CreateBizgroupDto {
    readonly groupname: string;
    readonly formAmount: number;
    readonly serviceCharge: number;
}
