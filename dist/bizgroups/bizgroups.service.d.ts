import { Model } from 'mongoose';
import { Bizgroup } from './bizgroup.interface';
import { CreateBizgroupDto } from './create-bizgroup.dto';
export declare class BizgroupsService {
    private readonly bizgrpModel;
    constructor(bizgrpModel: Model<Bizgroup>);
    private handleError;
    create(bizgrpDto: CreateBizgroupDto): Promise<Bizgroup>;
    findAll(): Promise<Bizgroup[]>;
    findOne(id: string): Promise<Bizgroup>;
    update(id: string): Promise<Bizgroup>;
    deleteOne(id: string): Promise<Bizgroup>;
}
