import { BizgroupsService } from './bizgroups.service';
import { CreateBizgroupDto } from './create-bizgroup.dto';
import { Bizgroup } from './bizgroup.interface';
export declare class BizgroupsController {
    private readonly bizgrpsService;
    constructor(bizgrpsService: BizgroupsService);
    create(createBizgroupDto: CreateBizgroupDto): void;
    findAll(): Promise<Bizgroup[]>;
    findOne(id: any): Promise<Bizgroup>;
    update(id: any, updatebizgrpDto: any): Promise<Bizgroup>;
    remove(id: any): Promise<Bizgroup>;
}
