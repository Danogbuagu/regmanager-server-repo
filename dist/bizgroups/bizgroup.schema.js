"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const mongoose_plugin_autoinc_1 = require("mongoose-plugin-autoinc");
exports.BizgroupSchema = new mongoose.Schema({
    groupname: { type: String, required: true },
    formAmount: { type: Number, required: true },
    serviceCharge: { type: Number, required: true },
});
exports.BizgroupSchema.plugin(mongoose_plugin_autoinc_1.autoIncrement, {
    model: 'Bizgroup',
    field: 'groupId',
    startAt: 101,
    incrementBy: 1,
});
//# sourceMappingURL=bizgroup.schema.js.map