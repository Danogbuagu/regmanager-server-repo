"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const validation_pipe_1 = require("../common/validation.pipe");
const bizgroups_service_1 = require("./bizgroups.service");
const create_bizgroup_dto_1 = require("./create-bizgroup.dto");
let BizgroupsController = class BizgroupsController {
    constructor(bizgrpsService) {
        this.bizgrpsService = bizgrpsService;
    }
    create(createBizgroupDto) {
        this.bizgrpsService.create(createBizgroupDto);
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.bizgrpsService.findAll();
        });
    }
    findOne(id) {
        return this.bizgrpsService.findOne(id);
    }
    update(id, updatebizgrpDto) {
        return this.bizgrpsService.update(id);
    }
    remove(id) {
        return this.bizgrpsService.deleteOne(id);
    }
};
__decorate([
    common_1.Post(),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_bizgroup_dto_1.CreateBizgroupDto]),
    __metadata("design:returntype", void 0)
], BizgroupsController.prototype, "create", null);
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], BizgroupsController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], BizgroupsController.prototype, "findOne", null);
__decorate([
    common_1.Put(':id'),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], BizgroupsController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], BizgroupsController.prototype, "remove", null);
BizgroupsController = __decorate([
    common_1.Controller('bizgroups'),
    __metadata("design:paramtypes", [bizgroups_service_1.BizgroupsService])
], BizgroupsController);
exports.BizgroupsController = BizgroupsController;
//# sourceMappingURL=bizgroups.controller.js.map