"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const bizgroups_controller_1 = require("./bizgroups.controller");
const bizgroups_service_1 = require("./bizgroups.service");
const mongoose_1 = require("@nestjs/mongoose");
const bizgroup_schema_1 = require("./bizgroup.schema");
let BizgroupsModule = class BizgroupsModule {
};
BizgroupsModule = __decorate([
    common_1.Module({
        imports: [mongoose_1.MongooseModule.forFeature([{ name: 'Bizgroup', schema: bizgroup_schema_1.BizgroupSchema }])],
        controllers: [bizgroups_controller_1.BizgroupsController],
        providers: [bizgroups_service_1.BizgroupsService],
    })
], BizgroupsModule);
exports.BizgroupsModule = BizgroupsModule;
//# sourceMappingURL=bizgroups.module.js.map