import { ReceiptsService } from './receipts.service';
import { CreateReceiptDto } from './create-receipt.dto';
import { Receipt } from './receipt.interface';
export declare class ReceiptsController {
    private readonly receiptsService;
    constructor(receiptsService: ReceiptsService);
    create(createReceiptDto: CreateReceiptDto): void;
    findAll(): Promise<Receipt[]>;
    update(id: any, updatereceiptDto: any): Promise<Receipt>;
    remove(id: any): Promise<Receipt>;
    findByDate(receiptDate: any): Promise<Receipt[]>;
}
