export declare class CreateReceiptDto {
    readonly sname: string;
    readonly fname: string;
    readonly onames: string;
    readonly phoneNo: string;
    readonly loggedInUser: string;
    readonly receiptDate: any;
    readonly formNo: string;
    readonly receiptNo: string;
    readonly amount: number;
    readonly jamb: number;
    readonly svc_charge: number;
    readonly group: string;
}
