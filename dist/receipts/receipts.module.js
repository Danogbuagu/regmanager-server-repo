"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const receipts_service_1 = require("./receipts.service");
const receipts_controller_1 = require("./receipts.controller");
const mongoose_1 = require("@nestjs/mongoose");
const receipt_schema_1 = require("./receipt.schema");
let ReceiptsModule = class ReceiptsModule {
};
ReceiptsModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([{ name: 'Receipt', schema: receipt_schema_1.ReceiptSchema }]),
        ],
        providers: [receipts_service_1.ReceiptsService],
        controllers: [receipts_controller_1.ReceiptsController],
    })
], ReceiptsModule);
exports.ReceiptsModule = ReceiptsModule;
//# sourceMappingURL=receipts.module.js.map