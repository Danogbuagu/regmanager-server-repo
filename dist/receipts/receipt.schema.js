"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.ReceiptSchema = new mongoose.Schema({
    sname: { type: String, required: true },
    fname: { type: String, required: true },
    onames: { type: String, required: '' },
    phoneNo: { type: String, required: true },
    loggedInUser: { type: String, required: true },
    receiptDate: { type: Date, default: Date.now },
    receiptNo: { type: String, required: true },
    formNo: { type: String, required: true },
    amount: { type: Number, required: true },
    jamb: { type: Number, required: true },
    svc_charge: { type: Number, required: true },
    date_created: { type: Date, default: Date.now },
    group: { type: String, required: true },
});
//# sourceMappingURL=receipt.schema.js.map