import { Model } from 'mongoose';
import { Receipt } from './receipt.interface';
import { CreateReceiptDto } from './create-receipt.dto';
export declare class ReceiptsService {
    private readonly receiptModel;
    constructor(receiptModel: Model<Receipt>);
    private handleError;
    create(receiptDto: CreateReceiptDto): Promise<Receipt>;
    findAll(): Promise<Receipt[]>;
    findOne(rctNo: string): Promise<Receipt>;
    update(id: string): Promise<Receipt>;
    deleteOne(id: string): Promise<Receipt>;
    findByDate(rcptDate: Date): Promise<Receipt[]>;
}
