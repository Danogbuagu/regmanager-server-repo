export declare class Receipt {
    readonly sname: string;
    readonly fname: string;
    readonly onames: string;
    readonly phoneNo: string;
    readonly receiptNo: string;
    readonly loggedInUser: string;
    readonly receiptDate: any;
    readonly formNo: string;
    readonly amount: number;
    readonly jamb: number;
    readonly svc_charge: number;
    readonly group: string;
}
