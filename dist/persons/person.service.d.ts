import { Model } from 'mongoose';
import { Person } from './person/person.interface';
import { CreatePersonDto } from './person/create-person.dto';
export declare class PersonService {
    private readonly personModel;
    constructor(personModel: Model<Person>);
    private handleError;
    create(personDto: CreatePersonDto): Promise<Person>;
    findAll(): Promise<Person[]>;
    findOne(phoneNo: string): Promise<Person>;
    update(id: string): Promise<Person>;
    deleteOne(id: string): Promise<Person>;
}
