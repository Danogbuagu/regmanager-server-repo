"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const mongoose_plugin_autoinc_1 = require("mongoose-plugin-autoinc");
exports.PersonSchema = new mongoose.Schema({
    sname: { type: String, required: true },
    fname: { type: String, required: true },
    onames: { type: String },
    phoneNo: { type: String },
    jambAmt: { type: Number, default: 0.0 },
    regAmt: { type: Number },
    dateVended: { type: Date, default: Date.now },
});
exports.PersonSchema.plugin(mongoose_plugin_autoinc_1.autoIncrement, {
    model: 'Person',
    field: 'personId',
    startAt: 1000,
    incrementBy: 1,
});
//# sourceMappingURL=person-schema.js.map