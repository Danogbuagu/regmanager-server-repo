import { CreatePersonDto } from './create-person.dto';
import { Person } from './person.interface';
import { PersonService } from '../person.service';
export declare class PersonController {
    private readonly personService;
    constructor(personService: PersonService);
    create(createPersonDto: CreatePersonDto): void;
    findAll(): Promise<Person[]>;
    findOne(id: any): Promise<Person>;
    update(id: any, updateuserDto: any): Promise<Person>;
    remove(id: any): Promise<Person>;
}
