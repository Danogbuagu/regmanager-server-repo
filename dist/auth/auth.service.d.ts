import { JwtService } from '@nestjs/jwt';
import { Model } from 'mongoose';
import { User } from '../users/user.interface';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
export declare class AuthService {
    private readonly jwtService;
    private readonly userModel;
    private saltRounds;
    constructor(jwtService: JwtService, userModel: Model<User>);
    getHash(password: string | undefined): Promise<string>;
    createUser(userDto: CreateUserDto): Promise<User>;
    compareHash(password: string | undefined, hash: string | undefined): Promise<boolean>;
    createToken(id: number, username: string): Promise<{
        expires_in: number;
        token: string;
    }>;
    getUserByUsername(username: string): Promise<User>;
    validateUser(signedUser: any): Promise<boolean>;
}
