import { AuthService } from './auth.service';
import { User } from '../users/user.interface';
export declare class AuthController {
    private readonly authService;
    constructor(authService: AuthService);
    loginUser(res: any, body: User): Promise<any>;
    registerUser(res: any, body: User): Promise<any>;
}
