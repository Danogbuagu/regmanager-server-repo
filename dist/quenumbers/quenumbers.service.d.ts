import { Model } from 'mongoose';
import { Quenumber } from './quenumber.interface';
import { CreateQuenumberDto } from './create-quenumber.dto';
export declare class QuenumbersService {
    private readonly queNbrModel;
    constructor(queNbrModel: Model<Quenumber>);
    private handleError;
    create(prtNbrDto: CreateQuenumberDto): Promise<Quenumber>;
    findAll(): Promise<Quenumber[]>;
    findOne(id: string): Promise<Quenumber>;
    update(id: string): Promise<Quenumber>;
    deleteOne(id: string): Promise<Quenumber>;
}
