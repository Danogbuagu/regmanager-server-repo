"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const create_quenumber_dto_1 = require("./create-quenumber.dto");
const quenumbers_service_1 = require("./quenumbers.service");
const common_1 = require("@nestjs/common");
let QuenumbersController = class QuenumbersController {
    constructor(queNbrsService) {
        this.queNbrsService = queNbrsService;
    }
    create(createQuenumberDto) {
        this.queNbrsService.create(createQuenumberDto);
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.queNbrsService.findAll();
        });
    }
    findOne(id) {
        return this.queNbrsService.findOne(id);
    }
    update(id, updatequeNbsDto) {
        return this.queNbrsService.update(id);
    }
    remove(id) {
        return this.queNbrsService.deleteOne(id);
    }
};
__decorate([
    common_1.Post(),
    common_1.UsePipes(new common_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_quenumber_dto_1.CreateQuenumberDto]),
    __metadata("design:returntype", void 0)
], QuenumbersController.prototype, "create", null);
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], QuenumbersController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], QuenumbersController.prototype, "findOne", null);
__decorate([
    common_1.Put(':id'),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], QuenumbersController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], QuenumbersController.prototype, "remove", null);
QuenumbersController = __decorate([
    common_1.Controller('quenumbers'),
    __metadata("design:paramtypes", [quenumbers_service_1.QuenumbersService])
], QuenumbersController);
exports.QuenumbersController = QuenumbersController;
//# sourceMappingURL=quenumbers.controller.js.map