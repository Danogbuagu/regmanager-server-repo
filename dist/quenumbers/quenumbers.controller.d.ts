import { CreateQuenumberDto } from './create-quenumber.dto';
import { QuenumbersService } from './quenumbers.service';
import { Quenumber } from './quenumber.interface';
export declare class QuenumbersController {
    private readonly queNbrsService;
    constructor(queNbrsService: QuenumbersService);
    create(createQuenumberDto: CreateQuenumberDto): void;
    findAll(): Promise<Quenumber[]>;
    findOne(id: any): Promise<Quenumber>;
    update(id: any, updatequeNbsDto: any): Promise<Quenumber>;
    remove(id: any): Promise<Quenumber>;
}
