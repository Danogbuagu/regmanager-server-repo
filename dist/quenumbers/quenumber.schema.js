"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const mongoose_plugin_autoinc_1 = require("mongoose-plugin-autoinc");
exports.QuenumberSchema = new mongoose.Schema({
    queno: { type: Number, required: true },
});
exports.QuenumberSchema.plugin(mongoose_plugin_autoinc_1.autoIncrement, {
    model: 'Quenumber',
    field: 'queueId',
    startAt: 1001,
    incrementBy: 1,
});
//# sourceMappingURL=quenumber.schema.js.map