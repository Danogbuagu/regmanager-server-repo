"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const users_module_1 = require("./users/users.module");
const mongoose_1 = require("@nestjs/mongoose");
const persons_module_1 = require("./persons/persons.module");
const bizgroups_module_1 = require("./bizgroups/bizgroups.module");
const quenumbers_module_1 = require("./quenumbers/quenumbers.module");
const prtnumbers_module_1 = require("./prtnumbers/prtnumbers.module");
const receipts_module_1 = require("./receipts/receipts.module");
const daily_expenses_module_1 = require("./daily-expenses/daily-expenses.module");
const pin_vending_module_1 = require("./pin-vending/pin-vending.module");
const auth_module_1 = require("./auth/auth.module");
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forRoot('mongodb://localhost/regdb', {
                useNewUrlParser: true,
                useCreateIndex: true,
                useFindAndModify: false,
            }),
            users_module_1.UsersModule,
            persons_module_1.PersonsModule,
            bizgroups_module_1.BizgroupsModule,
            quenumbers_module_1.QuenumbersModule,
            prtnumbers_module_1.PrtnumbersModule,
            receipts_module_1.ReceiptsModule,
            daily_expenses_module_1.DailyExpensesModule,
            pin_vending_module_1.PinVendingModule,
            auth_module_1.AuthModule,
        ],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map