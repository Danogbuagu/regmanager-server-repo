"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const daily_expenses_service_1 = require("./daily-expenses.service");
const common_1 = require("@nestjs/common");
const daily_expenses_dto_1 = require("./daily-expenses.dto");
const common_2 = require("@nestjs/common");
let DailyExpensesController = class DailyExpensesController {
    constructor(dailyExpService) {
        this.dailyExpService = dailyExpService;
    }
    create(dailyExpDto) {
        this.dailyExpService.create(dailyExpDto);
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.dailyExpService.findAll();
        });
    }
    findByDate(expenseDate) {
        let dtt = JSON.stringify(expenseDate);
        console.log(`Captured value of date arriving Controller(): ${dtt}`);
        dtt = JSON.parse(dtt);
        const yy = dtt.slice(0, 4);
        console.log(`Captured value of Year arriving Controller(): ${yy}`);
        const mm = dtt.slice(5, 7);
        console.log(`Captured value of Month arriving Controller(): ${mm}`);
        const dd = dtt.slice(8, 10);
        console.log(`Captured value of Day arriving Controller(): ${dd}`);
        const date = new Date(dtt);
        console.log(`Value of expenseDate USED IN  Controller: ${date}`);
        return this.dailyExpService.findByDate(date);
    }
    update(id, updatereceiptDto) {
        return this.dailyExpService.update(id);
    }
    remove(id) {
        return this.dailyExpService.deleteOne(id);
    }
};
__decorate([
    common_2.Post(),
    common_2.UsePipes(new common_1.ValidationPipe()),
    __param(0, common_2.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [daily_expenses_dto_1.DailyExpensesDto]),
    __metadata("design:returntype", void 0)
], DailyExpensesController.prototype, "create", null);
__decorate([
    common_2.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], DailyExpensesController.prototype, "findAll", null);
__decorate([
    common_2.Get(':expenseDate'),
    __param(0, common_2.Param('expenseDate')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], DailyExpensesController.prototype, "findByDate", null);
__decorate([
    common_2.Put(':id'),
    __param(0, common_2.Param('id')), __param(1, common_2.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], DailyExpensesController.prototype, "update", null);
__decorate([
    common_2.Delete(':id'),
    __param(0, common_2.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], DailyExpensesController.prototype, "remove", null);
DailyExpensesController = __decorate([
    common_2.Controller('daily-expenses'),
    __metadata("design:paramtypes", [daily_expenses_service_1.DailyExpensesService])
], DailyExpensesController);
exports.DailyExpensesController = DailyExpensesController;
//# sourceMappingURL=daily-expenses.controller.js.map