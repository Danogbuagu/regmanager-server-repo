"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
exports.DailyExpensesSchema = new mongoose.Schema({
    expenseDate: { type: Date, default: Date.now },
    category: { type: String, required: true },
    expenseDetails: { type: String, required: true },
    amount: { type: Number, required: true },
    loggedInUser: { type: String, required: '' },
    approvedBy: { type: String, required: true },
});
//# sourceMappingURL=daily-expenses.schema.js.map