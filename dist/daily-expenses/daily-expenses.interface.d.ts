export declare class DailyExpenses {
    readonly expenseDate: any;
    readonly category: string;
    readonly expenseDetails: string;
    readonly amount: number;
    readonly loggedInUser: string;
    readonly approvedBy: string;
}
