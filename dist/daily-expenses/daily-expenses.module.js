"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const daily_expenses_controller_1 = require("./daily-expenses.controller");
const daily_expenses_service_1 = require("./daily-expenses.service");
const mongoose_1 = require("@nestjs/mongoose");
const daily_expenses_schema_1 = require("./daily-expenses.schema");
let DailyExpensesModule = class DailyExpensesModule {
};
DailyExpensesModule = __decorate([
    common_1.Module({
        imports: [
            mongoose_1.MongooseModule.forFeature([
                { name: 'DailyExpenses', schema: daily_expenses_schema_1.DailyExpensesSchema },
            ]),
        ],
        controllers: [daily_expenses_controller_1.DailyExpensesController],
        providers: [daily_expenses_service_1.DailyExpensesService],
    })
], DailyExpensesModule);
exports.DailyExpensesModule = DailyExpensesModule;
//# sourceMappingURL=daily-expenses.module.js.map