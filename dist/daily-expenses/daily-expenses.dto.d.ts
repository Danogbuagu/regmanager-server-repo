export declare class DailyExpensesDto {
    expenseDate: any;
    category: string;
    expenseDetails: string;
    amount: number;
    loggedInUser: string;
    approvedBy: string;
}
