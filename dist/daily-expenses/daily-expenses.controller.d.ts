import { DailyExpensesService } from './daily-expenses.service';
import { DailyExpensesDto } from './daily-expenses.dto';
import { DailyExpenses } from './daily-expenses.interface';
export declare class DailyExpensesController {
    private readonly dailyExpService;
    constructor(dailyExpService: DailyExpensesService);
    create(dailyExpDto: DailyExpensesDto): void;
    findAll(): Promise<DailyExpenses[]>;
    findByDate(expenseDate: any): Promise<DailyExpenses[]>;
    update(id: any, updatereceiptDto: any): Promise<DailyExpenses>;
    remove(id: any): Promise<DailyExpenses>;
}
