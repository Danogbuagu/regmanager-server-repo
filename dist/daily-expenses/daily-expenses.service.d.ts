import { DailyExpenses } from './daily-expenses.interface';
import { Model } from 'mongoose';
import { DailyExpensesDto } from './daily-expenses.dto';
export declare class DailyExpensesService {
    private readonly dailyExpensesModel;
    constructor(dailyExpensesModel: Model<DailyExpenses>);
    private handleError;
    create(dailyExpDto: DailyExpensesDto): Promise<DailyExpenses>;
    findAll(): Promise<DailyExpenses[]>;
    update(id: string): Promise<DailyExpenses>;
    deleteOne(id: string): Promise<DailyExpenses>;
    findByDate(expDate: Date): Promise<DailyExpenses[]>;
}
