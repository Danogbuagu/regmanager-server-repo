"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const prtnumbers_service_1 = require("./prtnumbers.service");
const create_prtnumber_dto_1 = require("./create-prtnumber.dto");
let PrtnumbersController = class PrtnumbersController {
    constructor(prtNbrsService) {
        this.prtNbrsService = prtNbrsService;
    }
    create(createPrtnumberDto) {
        this.prtNbrsService.create(createPrtnumberDto);
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.prtNbrsService.findAll();
        });
    }
    findOne(id) {
        return this.prtNbrsService.findOne(id);
    }
    update(id, updateprtNbsDto) {
        return this.prtNbrsService.update(id);
    }
    remove(id) {
        return this.prtNbrsService.deleteOne(id);
    }
};
__decorate([
    common_1.Post(),
    common_1.UsePipes(new common_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_prtnumber_dto_1.CreatePrtnumberDto]),
    __metadata("design:returntype", void 0)
], PrtnumbersController.prototype, "create", null);
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], PrtnumbersController.prototype, "findAll", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], PrtnumbersController.prototype, "findOne", null);
__decorate([
    common_1.Put(':id'),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], PrtnumbersController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], PrtnumbersController.prototype, "remove", null);
PrtnumbersController = __decorate([
    common_1.Controller('prtnumbers'),
    __metadata("design:paramtypes", [prtnumbers_service_1.PrtnumbersService])
], PrtnumbersController);
exports.PrtnumbersController = PrtnumbersController;
//# sourceMappingURL=prtnumbers.controller.js.map