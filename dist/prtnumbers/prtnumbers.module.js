"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const prtnumbers_service_1 = require("./prtnumbers.service");
const prtnumbers_controller_1 = require("./prtnumbers.controller");
const prtnumber_schema_1 = require("./prtnumber.schema");
const mongoose_1 = require("@nestjs/mongoose");
let PrtnumbersModule = class PrtnumbersModule {
};
PrtnumbersModule = __decorate([
    common_1.Module({
        imports: [mongoose_1.MongooseModule.forFeature([{ name: 'Prtnumber', schema: prtnumber_schema_1.PrtnumberSchema }])],
        providers: [prtnumbers_service_1.PrtnumbersService],
        controllers: [prtnumbers_controller_1.PrtnumbersController]
    })
], PrtnumbersModule);
exports.PrtnumbersModule = PrtnumbersModule;
//# sourceMappingURL=prtnumbers.module.js.map