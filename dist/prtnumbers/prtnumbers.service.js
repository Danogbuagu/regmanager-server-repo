"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var _a;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const mongoose_2 = require("@nestjs/mongoose");
let PrtnumbersService = class PrtnumbersService {
    constructor(prtNbrModel) {
        this.prtNbrModel = prtNbrModel;
    }
    handleError(error) {
        return `A database error occured with the following result: ${error.message}`;
    }
    create(prtNbrDto) {
        return __awaiter(this, void 0, void 0, function* () {
            const newPrtnumber = new this.prtNbrModel(prtNbrDto);
            return yield newPrtnumber.save();
        });
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.prtNbrModel
                .find()
                .sort('queno')
                .exec();
        });
    }
    findOne(qno) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.prtNbrModel.find({ queno: qno }).exec();
        });
    }
    update(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.prtNbrModel.update({ id }).exec();
        });
    }
    deleteOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.prtNbrModel
                .deleteOne({ queno: parseInt(id, 10) }, err => {
                if (err)
                    return this.handleError(err);
            })
                .exec();
        });
    }
};
PrtnumbersService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_2.InjectModel('Prtnumber')),
    __metadata("design:paramtypes", [typeof (_a = typeof mongoose_1.Model !== "undefined" && mongoose_1.Model) === "function" ? _a : Object])
], PrtnumbersService);
exports.PrtnumbersService = PrtnumbersService;
//# sourceMappingURL=prtnumbers.service.js.map