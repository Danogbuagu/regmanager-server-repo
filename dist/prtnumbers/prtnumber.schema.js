"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const mongoose_plugin_autoinc_1 = require("mongoose-plugin-autoinc");
exports.PrtnumberSchema = new mongoose.Schema({
    queno: { type: Number, required: true },
});
exports.PrtnumberSchema.plugin(mongoose_plugin_autoinc_1.autoIncrement, {
    model: 'Prtnumber',
    field: 'prtnumberId',
    startAt: 1001,
    incrementBy: 1,
});
//# sourceMappingURL=prtnumber.schema.js.map