import { Model } from 'mongoose';
import { CreatePrtnumberDto } from './create-prtnumber.dto';
import { Prtnumber } from './prtnumber.interface';
export declare class PrtnumbersService {
    private readonly prtNbrModel;
    constructor(prtNbrModel: Model<Prtnumber>);
    private handleError;
    create(prtNbrDto: CreatePrtnumberDto): Promise<Prtnumber>;
    findAll(): Promise<Prtnumber[]>;
    findOne(qno: number): Promise<Prtnumber>;
    update(id: string): Promise<Prtnumber>;
    deleteOne(id: string): Promise<Prtnumber>;
}
