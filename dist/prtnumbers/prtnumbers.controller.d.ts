import { PrtnumbersService } from './prtnumbers.service';
import { CreatePrtnumberDto } from './create-prtnumber.dto';
import { Prtnumber } from './prtnumber.interface';
export declare class PrtnumbersController {
    private readonly prtNbrsService;
    constructor(prtNbrsService: PrtnumbersService);
    create(createPrtnumberDto: CreatePrtnumberDto): void;
    findAll(): Promise<Prtnumber[]>;
    findOne(id: any): Promise<Prtnumber>;
    update(id: any, updateprtNbsDto: any): Promise<Prtnumber>;
    remove(id: any): Promise<Prtnumber>;
}
