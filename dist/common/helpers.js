"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Helpers {
    constructor() { }
    getMonthNameFrom(moStr) {
        let retStr;
        switch (moStr) {
            case '01':
                retStr = 'Jan';
                break;
            case '02':
                retStr = 'Feb';
                break;
            case '03':
                retStr = 'Mar';
                break;
            case '04':
                retStr = 'Apr';
                break;
            case '05':
                retStr = 'May';
                break;
            case '06':
                retStr = 'Jun';
                break;
            case '07':
                retStr = 'Jul';
                break;
            case '08':
                retStr = 'Aug';
                break;
            case '09':
                retStr = 'Sep';
                break;
            case '10':
                retStr = 'Oct';
                break;
            case '11':
                retStr = 'Nov';
                break;
            case '12':
                retStr = 'Dec';
                break;
            default:
                retStr = 'invalid month';
        }
        return retStr;
    }
    isoToDateObject(isoDateStr) {
        let dtt = JSON.stringify(isoDateStr);
        console.log(`Captured value of date arriving Controller(): ${dtt}`);
        dtt = JSON.parse(dtt);
        const yy = dtt.slice(0, 4);
        console.log(`Captured value of Year arriving Controller(): ${yy}`);
        const mm = dtt.slice(5, 7);
        console.log(`Captured value of Year arriving Controller(): ${mm}`);
        const dd = dtt.slice(8, 10);
        console.log(`Captured value of Year arriving Controller(): ${dd}`);
        console.log(`Value of receiptDate arriving Controller: ${dtt}`);
        const date = new Date(parseInt(yy, 10), parseInt(mm, 10), parseInt(dd, 10));
        return date;
    }
}
exports.Helpers = Helpers;
//# sourceMappingURL=helpers.js.map