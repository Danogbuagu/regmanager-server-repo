export declare class Helpers {
    constructor();
    getMonthNameFrom(moStr: string): string;
    isoToDateObject(isoDateStr: string): Date;
}
