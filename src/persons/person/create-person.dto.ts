import { IsString, IsInt, IsDate, IsNumber, IsBoolean } from 'class-validator';

export class CreatePersonDto {
  @IsString()
  readonly sname: string;
  @IsString()
  readonly fname: string;
  @IsString()
  readonly onames: string;
  @IsString()
  readonly phoneNo: string;
  @IsNumber()
  readonly jambAmt?: number;
  @IsNumber()
  readonly regAmt?: number;
}
