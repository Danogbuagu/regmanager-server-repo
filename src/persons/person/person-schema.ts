import * as mongoose from 'mongoose';
import { autoIncrement } from 'mongoose-plugin-autoinc';

export const PersonSchema = new mongoose.Schema({
  sname: { type: String, required: true },
  fname: { type: String, required: true },
  onames: { type: String },
  phoneNo: { type: String },
  jambAmt: { type: Number, default: 0.0 },
  regAmt: { type: Number },
  dateVended: { type: Date, default: Date.now },
});

PersonSchema.plugin(autoIncrement, {
  model: 'Person',
  field: 'personId',
  startAt: 1000,
  incrementBy: 1,
});
