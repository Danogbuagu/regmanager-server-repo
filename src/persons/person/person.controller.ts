import {
  Controller,
  Post,
  UsePipes,
  Body,
  Get,
  Put,
  Delete,
  Param,
  ValidationPipe,
} from '@nestjs/common';
import { CreatePersonDto } from './create-person.dto';
import { Person } from './person.interface';
import { PersonService } from '../person.service';

@Controller('person')
export class PersonController {
  constructor(private readonly personService: PersonService) { }

  @Post()
  // @Roles('admin')
  @UsePipes(new ValidationPipe())
  create(@Body() createPersonDto: CreatePersonDto) {
    this.personService.create(createPersonDto);
  }
  /*
curl -X POST -H 'content-type: application/json' -d '{
    "id": 1000,
    "username": "johnbull",
    "sname": "Bull",
    "fname": "John",
    "onames": "Shit",
    "phoneNo": "00055556666",
    "role": "user",
    "active":  true,
    "visits": 0
  }' localhost:3000/users

*/
  // @Get()
  // findAll(@Query() query) {
  //   return `This action returns all users (limit: ${query.limit} items)`;
  // }

  @Get()
  async findAll(): Promise<Person[]> {
    return this.personService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id) {
    return this.personService.findOne(id); // `This action returns a #${id} user`;
  }

  @Put(':id')
  update(@Param('id') id, @Body() updateuserDto) {
    return this.personService.update(id); // `This action updates a #${id} user`;
  }

  @Delete(':id')
  remove(@Param('id') id) {
    return this.personService.deleteOne(id); // `This action removes a #${id} user`;
  }
}
