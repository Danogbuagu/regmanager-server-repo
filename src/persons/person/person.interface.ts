export class Person {
  readonly sname: string;
  readonly fname: string;
  readonly onames: string;
  readonly phoneNo: string;
  readonly jambAmt?: number;
  readonly regAmt?: number;
}
