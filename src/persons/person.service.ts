import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Person } from './person/person.interface';
import { CreatePersonDto } from './person/create-person.dto';

@Injectable()
export class PersonService {

  constructor(@InjectModel('Person') private readonly personModel: Model<Person>) {}

  private handleError(error: Error): string {
    return `A database error occured with the following result: ${
      error.message
    }`;
  }

  async create(personDto: CreatePersonDto): Promise<Person> {
    const newPerson = new this.personModel(personDto);
    return await newPerson.save();
  }

  async findAll(): Promise<Person[]> {
    return await this.personModel
    .find()
    .sort('phoneNo')
    .exec();
  }

  async findOne(phoneNo: string): Promise<Person> {
    return await this.personModel.find({phoneNo})
      .exec();
  }

  async update(id: string): Promise<Person> {
    return await this.personModel.update({ id }).exec();
  }

  async deleteOne(id: string): Promise<Person> {
    return await this.personModel.remove({ id }).exec();
  }
}
