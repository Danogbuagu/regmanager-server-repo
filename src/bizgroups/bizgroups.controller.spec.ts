import { Test, TestingModule } from '@nestjs/testing';
import { BizgroupsController } from './bizgroups.controller';

describe('Bizgroups Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [BizgroupsController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: BizgroupsController = module.get<BizgroupsController>(BizgroupsController);
    expect(controller).toBeDefined();
  });
});
