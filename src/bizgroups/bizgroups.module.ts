import { Module } from '@nestjs/common';
import { BizgroupsController } from './bizgroups.controller';
import { BizgroupsService } from './bizgroups.service';
import { MongooseModule } from '@nestjs/mongoose';
import { BizgroupSchema } from './bizgroup.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Bizgroup', schema: BizgroupSchema }])],
  controllers: [BizgroupsController],
  providers: [BizgroupsService],
})
export class BizgroupsModule {}
