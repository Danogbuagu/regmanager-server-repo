import { Test, TestingModule } from '@nestjs/testing';
import { BizgroupsService } from './bizgroups.service';

describe('BizgroupsService', () => {
  let service: BizgroupsService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BizgroupsService],
    }).compile();
    service = module.get<BizgroupsService>(BizgroupsService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
