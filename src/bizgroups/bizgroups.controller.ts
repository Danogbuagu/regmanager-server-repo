import {
  Controller,
  Post,
  UsePipes,
  Body,
  Get,
  Param,
  Put,
  Delete,
} from '@nestjs/common';
import { ValidationPipe } from '../common/validation.pipe';
import { BizgroupsService } from './bizgroups.service';
import { CreateBizgroupDto } from './create-bizgroup.dto';
import { Bizgroup } from './bizgroup.interface';

@Controller('bizgroups')
export class BizgroupsController {
  constructor(private readonly bizgrpsService: BizgroupsService) {}

  @Post()
  // @Roles('admin')
  @UsePipes(new ValidationPipe())
  create(@Body() createBizgroupDto: CreateBizgroupDto) {
    this.bizgrpsService.create(createBizgroupDto);
  }
  /*
curl -X POST -H 'content-type: application/json' -d '{
    "id": 1000,
    "username": "johnbull",
    "sname": "Bull",
    "fname": "John",
    "onames": "Shit",
    "phoneNo": "00055556666",
    "role": "user",
    "active":  true,
    "visits": 0
  }' localhost:3000/users

*/
  // @Get()
  // findAll(@Query() query) {
  //   return `This action returns all users (limit: ${query.limit} items)`;
  // }

  @Get()
  async findAll(): Promise<Bizgroup[]> {
    return this.bizgrpsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id) {
    return this.bizgrpsService.findOne(id); // `This action returns a #${id} user`;
  }

  @Put(':id')
  update(@Param('id') id, @Body() updatebizgrpDto) {
    return this.bizgrpsService.update(id); // `This action updates a #${id} user`;
  }

  @Delete(':id')
  remove(@Param('id') id) {
    return this.bizgrpsService.deleteOne(id); // `This action removes a #${id} user`;
  }
}
