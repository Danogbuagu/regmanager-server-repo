import * as mongoose from 'mongoose';
import { autoIncrement } from 'mongoose-plugin-autoinc';

export const BizgroupSchema = new mongoose.Schema({
  groupname: { type: String, required: true },
  formAmount: { type: Number, required: true },
  serviceCharge: { type: Number, required: true },
});

BizgroupSchema.plugin(autoIncrement, {
  model: 'Bizgroup',
  field: 'groupId',
  startAt: 101,
  incrementBy: 1,
});
