import { IsString, IsNumber } from 'class-validator';

export class CreateBizgroupDto {
  @IsString()
  readonly groupname: string;
  @IsNumber()
  readonly formAmount: number;
  @IsNumber()
  readonly serviceCharge: number;
}
