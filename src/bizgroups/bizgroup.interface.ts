export class Bizgroup {
  readonly groupname: string;
  readonly formAmount: number;
  readonly serviceCharge: number;
}
