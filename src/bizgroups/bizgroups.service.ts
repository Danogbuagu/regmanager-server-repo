import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Bizgroup } from './bizgroup.interface';
import { CreateBizgroupDto } from './create-bizgroup.dto';

@Injectable()
export class BizgroupsService {
  constructor(
    @InjectModel('Bizgroup') private readonly bizgrpModel: Model<Bizgroup>,
  ) {}

  private handleError(error: Error): string {
    return `A database error occured with the following result: ${
      error.message
    }`;
  }

  async create(bizgrpDto: CreateBizgroupDto): Promise<Bizgroup> {
    const newBizgroup = new this.bizgrpModel(bizgrpDto);
    return await newBizgroup.save();
  }

  async findAll(): Promise<Bizgroup[]> {
    return await this.bizgrpModel.find().exec();
  }

  async findOne(id: string): Promise<Bizgroup> {
    return await this.bizgrpModel.find
      .where('id')
      .equals(id)
      .exec();
  }

  async update(id: string): Promise<Bizgroup> {
    return await this.bizgrpModel.update({ id }).exec();
  }

  async deleteOne(id: string): Promise<Bizgroup> {
    return await this.bizgrpModel.remove({ id }).exec();
  }
}
