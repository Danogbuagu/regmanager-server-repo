import { Module } from '@nestjs/common';
import { DailyExpensesController } from './daily-expenses.controller';
import { DailyExpensesService } from './daily-expenses.service';
import { MongooseModule } from '@nestjs/mongoose';
import { DailyExpensesSchema } from './daily-expenses.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'DailyExpenses', schema: DailyExpensesSchema },
    ]),
  ],
  controllers: [DailyExpensesController],
  providers: [DailyExpensesService],
})
export class DailyExpensesModule {}
