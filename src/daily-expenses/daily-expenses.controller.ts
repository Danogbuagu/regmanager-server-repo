import { DailyExpensesService } from './daily-expenses.service';
import { ValidationPipe } from '@nestjs/common';
import { DailyExpensesDto } from './daily-expenses.dto';
import { DailyExpenses } from './daily-expenses.interface';
import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Put,
  Delete,
  UsePipes,
} from '@nestjs/common';

@Controller('daily-expenses')
export class DailyExpensesController {
  constructor(private readonly dailyExpService: DailyExpensesService) {}

  @Post()
  // @Roles('admin')
  @UsePipes(new ValidationPipe())
  create(@Body() dailyExpDto: DailyExpensesDto) {
    this.dailyExpService.create(dailyExpDto);
  }

  @Get()
  async findAll(): Promise<DailyExpenses[]> {
    return this.dailyExpService.findAll();
  }

  @Get(':expenseDate')
  findByDate(@Param('expenseDate') expenseDate): Promise<DailyExpenses[]> {
    let dtt = JSON.stringify(expenseDate);
    // tslint:disable-next-line:no-console
    console.log(`Captured value of date arriving Controller(): ${dtt}`);
    dtt = JSON.parse(dtt);
    // tslint:disable-next-line:no-string-literal
    const yy = dtt.slice(0, 4);
    // tslint:disable-next-line:no-console
    console.log(`Captured value of Year arriving Controller(): ${yy}`);
    const mm = dtt.slice(5, 7);
    // tslint:disable-next-line:no-console
    console.log(`Captured value of Month arriving Controller(): ${mm}`);
    const dd = dtt.slice(8, 10);
    // tslint:disable-next-line:no-console
    console.log(`Captured value of Day arriving Controller(): ${dd}`);

    const date = new Date(dtt);
    // const date = new Helpers().isoToDateObject(dtt);
    // tslint:disable-next-line:no-console
    console.log(`Value of expenseDate USED IN  Controller: ${date}`);
    return this.dailyExpService.findByDate(date);
  }

  @Put(':id')
  update(@Param('id') id, @Body() updatereceiptDto) {
    return this.dailyExpService.update(id); // `This action updates a #${id} receipt`;
  }

  @Delete(':id')
  remove(@Param('id') id) {
    return this.dailyExpService.deleteOne(id); // `This action removes a #${id} receipt`;
  }
}
