import * as mongoose from 'mongoose';

export const DailyExpensesSchema = new mongoose.Schema({
  expenseDate: { type: Date, default: Date.now },
  category: { type: String, required: true },
  expenseDetails: { type: String, required: true },
  amount: { type: Number, required: true },
  loggedInUser: { type: String, required: '' },
  approvedBy: { type: String, required: true },
});
