import { IsString, IsNumber } from 'class-validator';
export class DailyExpensesDto {
  expenseDate: any;
  @IsString()
  category: string;
  @IsString()
  expenseDetails: string;
  @IsNumber()
  amount: number;
  @IsString()
  loggedInUser: string;
  @IsString()
  approvedBy: string;
}
