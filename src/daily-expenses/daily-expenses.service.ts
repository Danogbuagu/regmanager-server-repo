import { Injectable, Param, Get } from '@nestjs/common';
import { DailyExpenses } from './daily-expenses.interface';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { DailyExpensesDto } from './daily-expenses.dto';

@Injectable()
export class DailyExpensesService {
  constructor(
    @InjectModel('DailyExpenses')
    private readonly dailyExpensesModel: Model<DailyExpenses>,
  ) {}

  private handleError(error: Error): string {
    return `A database error occured with the following result: ${
      error.message
    }`;
  }

  async create(dailyExpDto: DailyExpensesDto): Promise<DailyExpenses> {
    const newExp = new this.dailyExpensesModel(dailyExpDto);
    return await newExp.save();
  }

  async findAll(): Promise<DailyExpenses[]> {
    return await this.dailyExpensesModel.find().exec();
  }

  async update(id: string): Promise<DailyExpenses> {
    return await this.dailyExpensesModel.update({ id }).exec();
  }

  async deleteOne(id: string): Promise<DailyExpenses> {
    return await this.dailyExpensesModel.remove({ id }).exec();
  }

  async findByDate(expDate: Date): Promise<DailyExpenses[]> {
    return await this.dailyExpensesModel
      .find({ expenseDate: expDate })
      .sort('expenseDate')
      .exec();
  }
}
