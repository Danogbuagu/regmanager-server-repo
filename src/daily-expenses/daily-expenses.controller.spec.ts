import { Test, TestingModule } from '@nestjs/testing';
import { DailyExpensesController } from './daily-expenses.controller';

describe('DailyExpenses Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [DailyExpensesController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: DailyExpensesController = module.get<DailyExpensesController>(DailyExpensesController);
    expect(controller).toBeDefined();
  });
});
