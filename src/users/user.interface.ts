export class User {
  readonly email: string;
  readonly username: string;
  readonly sname: string;
  readonly fname: string;
  readonly onames: string;
  readonly phoneNo: string;
  readonly roles: string[];
  readonly active: boolean;
  password: string;
  passwordHash: string;
  userId: number;
}
