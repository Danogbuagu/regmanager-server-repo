import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './user.interface';
import {
  Controller,
  Post,
  UsePipes,
  Body,
  Get,
  Put,
  Delete,
  Param,
  ValidationPipe,
} from '@nestjs/common';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  //  @UseGuards(AuthGuard('jwt'), RolesGuard)
  //  @Roles('admin')
  @UsePipes(new ValidationPipe())
  async create(@Body() createUserDto: CreateUserDto) {

      await this.usersService.createUser(createUserDto);
 }

  @Get()
  async findAll(): Promise<User[]> {
    return this.usersService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id) {
    return this.usersService.findOne(id); // `This action returns a #${id} user`;
  }

  @Put(':id')
  update(@Param('id') id) {
    return this.usersService.update(id); // `This action updates a #${id} user`;
  }

  @Delete(':id')
  remove(@Param('id') id) {
    return this.usersService.deleteOne(id); // `This action removes a #${id} user`;
  }
}
