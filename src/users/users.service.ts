import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './user.interface';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {

  private saltRounds = 10;

  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  async getHash(password: string | undefined): Promise<string> {
    return bcrypt.hash(password, this.saltRounds);
  }

  async compareHash(
    password: string | undefined,
    hash: string | undefined,
  ): Promise<boolean> {
    return bcrypt.compare(password, hash);
  }

  async createUser(userDto: CreateUserDto): Promise<User> {
    const newUser = new this.userModel(userDto);
    newUser.passwordHash = await this.getHash(newUser.password);

    // clear password as we don't persist passwords
    // newUser.password = undefined;

    return await newUser.save();
  }

  async findAll(): Promise<User[]> {
    return await this.userModel
      .find()
      .sort('phoneNo')
      .exec();
  }

  async findOne(phoneNo: string): Promise<User> {
    return await this.userModel.find({ phoneNo }).exec();
  }

  async getUserByUsername(username: string): Promise<User> {
    return (await this.userModel.find({ username })).exec();
  }

  async update(id: string): Promise<User> {
    return await this.userModel.update({ id }).exec();
  }

  async deleteOne(id: string): Promise<User> {
    return await this.userModel.remove({ id }).exec();
  }

  async findOneByToken(token: string): Promise<User> {
    return await this.userModel.find({ token }).exec();
  }

  async getUserByEmail(email: string): Promise<User> {
    return await this.userModel.find({ email }).exec();
  }

  async findOneByEmail(email: any): Promise<User> {
    return await this.userModel.find({ email }).exec();
  }
}
