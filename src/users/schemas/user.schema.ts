import * as mongoose from 'mongoose';
import { autoIncrement } from 'mongoose-plugin-autoinc';
import * as bcrypt from 'bcrypt';

export const UserSchema = new mongoose.Schema(
  {
    username: { type: String, unique: true, required: true },
    email: { type: String, required: true, unique: true, lowercase: true },
    sname: { type: String, required: true },
    fname: { type: String, required: true },
    onames: { type: String, default: '' },
    phoneNo: { type: String, default: '' },
    roles: { type: [''], required: true, default: ['user'] },
    active: { type: Boolean, default: true },
    password: { type: String, default: '' },
    passwordHash: { type: String, default: '' },
    // userId: { type: Number, default: 0 },
  },
  {
    timestamps: true,
  },
);

UserSchema.plugin(autoIncrement, {
  model: 'User',
  field: 'userId',
  startAt: 1000,
  incrementBy: 1,
});

