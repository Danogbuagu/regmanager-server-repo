import { IsString, IsInt, IsDate, IsNumber, IsBoolean, IsArray, IsEmail, IsNotEmpty } from 'class-validator';

export class CreateUserDto {
  @IsString()
  readonly username: string;
  @IsString()
  readonly email: string;
  @IsString()
  readonly sname: string;
  @IsString()
  readonly fname: string;
  @IsString()
  readonly onames: string;
  @IsString()
  readonly phoneNo: string;
  @IsArray()
  readonly roles: string[];

  @IsBoolean()
  readonly active: boolean;
  @IsString()
  @IsNotEmpty()
  password: string;
}
