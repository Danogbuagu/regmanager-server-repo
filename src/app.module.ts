// import { Module } from '@nestjs/common';
// import { AppController } from './app.controller';
// import { AppService } from './app.service';

// @Module({
//   imports: [],
//   controllers: [AppController],
//   providers: [AppService],
// })
// export class AppModule {}
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { MongooseModule } from '@nestjs/mongoose';
import { PersonsModule } from './persons/persons.module';
import { BizgroupsModule } from './bizgroups/bizgroups.module';
import { QuenumbersModule } from './quenumbers/quenumbers.module';
import { PrtnumbersModule } from './prtnumbers/prtnumbers.module';
import { ReceiptsModule } from './receipts/receipts.module';
import { DailyExpensesModule } from './daily-expenses/daily-expenses.module';
import { PinVendingModule } from './pin-vending/pin-vending.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/regdb', {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
    }),
    UsersModule,
    PersonsModule,
    BizgroupsModule,
    QuenumbersModule,
    PrtnumbersModule,
    ReceiptsModule,
    DailyExpensesModule,
    PinVendingModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
