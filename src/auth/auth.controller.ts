import { Response, Body } from '@nestjs/common/decorators/http';
import { Controller, Post, HttpStatus } from '@nestjs/common';
import { AuthService } from './auth.service';
import { User } from '../users/user.interface';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
  )
  {}

  @Post('login')
  async loginUser(@Response() res: any, @Body() body: User) {
    if (!(body && body.username && body.password)) {
      return res
        .status(HttpStatus.FORBIDDEN)
        .json({ message: 'Username and password are required!' });
    }

    const user = await this.authService.getUserByUsername(body.username);

    if (user) {
      if (
        await this.authService.compareHash(body.password, user.passwordHash)
      ) {
        return res
          .status(HttpStatus.OK)
          .json(await this.authService.createToken(user.userId, user.username));
      }
    }

    return res
      .status(HttpStatus.FORBIDDEN)
      .json({ message: 'Username or password wrong!' });
  }

  @Post('register')
  async registerUser(@Response() res: any, @Body() body: User) {
    if (!(body && body.username && body.password)) {
      return res
        .status(HttpStatus.FORBIDDEN)
        .json({ message: 'Username and password are required!' });
    }

    let user = await this.authService.getUserByUsername(body.username);

    if (user) {
       return res.status(HttpStatus.FORBIDDEN).json({ message: 'Username exists' });
     } else {
       user = await this.authService.createUser(body);
       if (user) {
         user.passwordHash = undefined;
       }
     }

    //  return res.status(HttpStatus.OK).json(user);
  }
}
