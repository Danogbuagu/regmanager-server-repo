import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from '../users/user.interface';
import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcrypt';
import { CreateUserDto } from 'src/users/dto/create-user.dto';

@Injectable()
export class AuthService {
  private saltRounds = 10;

  constructor(
    private readonly jwtService: JwtService,
    @InjectModel('User') private readonly userModel: Model<User>,
  ) {}

  //   async createToken() {
  //     const user: JwtPayload = { email: 'test@email.com' };
  //     const accessToken = this.jwtService.sign(user);
  //     return {
  //       expiresIn: 3600,
  //       accessToken,
  //     };
  //   }

  //   async validateUser(payload: JwtPayload): Promise<any> {
  //     // put some validation logic here
  //     // for example query user by id/email/username
  //     return {};
  //   }

  //   async getUserByEmail(email: string): Promise<User> {
  //     return await this.userModel.find({ email }).exec();
  //   }

  async getHash(password: string | undefined): Promise<string> {
    return bcrypt.hash(password, this.saltRounds);
  }

  async createUser(userDto: CreateUserDto): Promise<User> {
    const newUser = new this.userModel(userDto);
    newUser.passwordHash = await this.getHash(newUser.password);

    // clear password as we don't persist passwords
    // newUser.password = undefined;

    return await newUser.save();
  }

  async compareHash(
    password: string | undefined,
    hash: string | undefined,
  ): Promise<boolean> {
    return bcrypt.compare(password, hash);
  }

  async createToken(id: number, username: string) {
    const expiresIn = 60 * 60;
    const secretOrKey = 'QuickBrownFox51';
    const user = { username };
    const token = jwt.sign(user, secretOrKey, { expiresIn });

    return { expires_in: expiresIn, token };
  }

  async getUserByUsername(username: string): Promise<User> {
    return (await this.userModel.find({ username }))[0];
  }

  async validateUser(signedUser): Promise<boolean> {
    if (signedUser && signedUser.username) {
      return Boolean(this.getUserByUsername(signedUser.username));
    }

    return false;
  }
}
