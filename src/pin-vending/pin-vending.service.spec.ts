import { Test, TestingModule } from '@nestjs/testing';
import { PinVendingService } from './pin-vending.service';

describe('PinVendingService', () => {
  let service: PinVendingService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PinVendingService],
    }).compile();
    service = module.get<PinVendingService>(PinVendingService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
