import { Test, TestingModule } from '@nestjs/testing';
import { PinVendingController } from './pin-vending.controller';

describe('PinVending Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [PinVendingController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: PinVendingController = module.get<PinVendingController>(PinVendingController);
    expect(controller).toBeDefined();
  });
});
