import { Module } from '@nestjs/common';
import { PinVendingController } from './pin-vending.controller';
import { PinVendingService } from './pin-vending.service';

@Module({
  controllers: [PinVendingController],
  providers: [PinVendingService]
})
export class PinVendingModule {}
