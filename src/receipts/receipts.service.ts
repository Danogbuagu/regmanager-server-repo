import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Receipt } from './receipt.interface';
import { CreateReceiptDto } from './create-receipt.dto';

@Injectable()
export class ReceiptsService {
  // private readonly receipts: Receipt[] = [];

  constructor(
    @InjectModel('Receipt') private readonly receiptModel: Model<Receipt>,
  ) {}

  private handleError(error: Error): string {
    return `A database error occured with the following result: ${
      error.message
    }`;
  }

  async create(receiptDto: CreateReceiptDto): Promise<Receipt> {
    const newReceipt = new this.receiptModel(receiptDto);
    return await newReceipt.save();
  }

  async findAll(): Promise<Receipt[]> {
    return await this.receiptModel.find().exec();
  }

  async findOne(rctNo: string): Promise<Receipt> {
    return await this.receiptModel.find({ receiptNo: rctNo }).exec();
  }

  async update(id: string): Promise<Receipt> {
    return await this.receiptModel.update({ id }).exec();
  }

  async deleteOne(id: string): Promise<Receipt> {
    return await this.receiptModel.remove({ id }).exec();
  }

  async findByDate(rcptDate: Date): Promise<Receipt[]> {
    return await this.receiptModel
      .find({ receiptDate: rcptDate })
      .sort('receiptDate')
      .exec();
  }
}
