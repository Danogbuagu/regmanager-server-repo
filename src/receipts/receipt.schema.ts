import * as mongoose from 'mongoose';

export const ReceiptSchema = new mongoose.Schema({
  sname: { type: String, required: true },
  fname: { type: String, required: true },
  onames: { type: String, required: '' },
  phoneNo: { type: String, required: true },
  loggedInUser: { type: String, required: true },
  receiptDate: { type: Date, default: Date.now },
  receiptNo: { type: String, required: true },
  formNo: { type: String, required: true },
  amount: { type: Number, required: true },
  jamb: { type: Number, required: true },
  svc_charge: { type: Number, required: true },
  date_created: { type: Date, default: Date.now },
  group: { type: String, required: true },
});

// ReceiptSchema.plugin(autoIncrement, {
//   model: 'Receipt',
//   field: 'receiptNo',
//   startAt: 1001,
//   incrementBy: 1,
// });
