import { Test, TestingModule } from '@nestjs/testing';
import { ReceiptsController } from './receipts.controller';

describe('Receipts Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [ReceiptsController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: ReceiptsController = module.get<ReceiptsController>(ReceiptsController);
    expect(controller).toBeDefined();
  });
});
