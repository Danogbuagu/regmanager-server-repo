import { IsString, IsNumber } from 'class-validator';

export class CreateReceiptDto {
  @IsString()
  readonly sname: string;
  @IsString()
  readonly fname: string;
  @IsString()
  readonly onames: string;
  @IsString()
  readonly phoneNo: string;
  @IsString()
  readonly loggedInUser: string;
  readonly receiptDate: any;
  @IsString()
  readonly formNo: string;
  @IsString()
  readonly receiptNo: string;  
  @IsNumber()
  readonly amount: number;
  @IsNumber()
  readonly jamb: number;
  @IsNumber()
  readonly svc_charge: number;
  @IsString()
  readonly group: string;
}
