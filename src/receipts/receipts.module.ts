import { Module } from '@nestjs/common';
import { ReceiptsService } from './receipts.service';
import { ReceiptsController } from './receipts.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ReceiptSchema } from './receipt.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Receipt', schema: ReceiptSchema }]),
  ],
  providers: [ReceiptsService],
  controllers: [ReceiptsController],
})
export class ReceiptsModule {}
