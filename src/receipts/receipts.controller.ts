import { ValidationPipe } from '@nestjs/common';
import { ReceiptsService } from './receipts.service';
import { CreateReceiptDto } from './create-receipt.dto';
import { Receipt } from './receipt.interface';
import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Put,
  Delete,
  UsePipes,
} from '@nestjs/common';

@Controller('receipts')
export class ReceiptsController {
  constructor(private readonly receiptsService: ReceiptsService) {}

  @Post()
  // @Roles('admin')
  @UsePipes(new ValidationPipe())
  create(@Body() createReceiptDto: CreateReceiptDto) {
    this.receiptsService.create(createReceiptDto);
  }
  /*
curl -X POST -H 'content-type: application/json' -d '{
  "id": 1000,
  "receiptname": "johnbull",
  "sname": "Bull",
  "fname": "John",
  "onames": "Shit",
  "phoneNo": "00055556666",
  "role": "receipt",
  "active":  true,
  "visits": 0
}' localhost:3000/receipts

*/

  @Get()
  async findAll(): Promise<Receipt[]> {
    return this.receiptsService.findAll();
  }

  // @Get(':id')
  // findOne(@Param('id') id) {
  //   return this.receiptsService.findOne(id); // `This action returns a #${id} receipt`;
  // }

  @Put(':id')
  update(@Param('id') id, @Body() updatereceiptDto) {
    return this.receiptsService.update(id); // `This action updates a #${id} receipt`;
  }

  @Delete(':id')
  remove(@Param('id') id) {
    return this.receiptsService.deleteOne(id); // `This action removes a #${id} receipt`;
  }

  @Get(':receiptDate')
  findByDate(@Param('receiptDate') receiptDate): Promise<Receipt[]> {
    let dtt = JSON.stringify(receiptDate);
    // tslint:disable-next-line:no-console
    console.log(`Captured value of date arriving Controller(): ${dtt}`);
    dtt = JSON.parse(dtt);
    // tslint:disable-next-line:no-string-literal
    const yy = dtt.slice(0, 4);
    // tslint:disable-next-line:no-console
    console.log(`Captured value of Year arriving Controller(): ${yy}`);
    const mm = dtt.slice(5, 7);
    // tslint:disable-next-line:no-console
    console.log(`Captured value of Year arriving Controller(): ${mm}`);
    const dd = dtt.slice(8, 10);
    // tslint:disable-next-line:no-console
    console.log(`Captured value of Year arriving Controller(): ${dd}`);

    // tslint:disable-next-line:no-console
    console.log(`Value of receiptDate arriving Controller: ${dtt}`);

    const date = new Date(dtt);
   // const date = new Helpers().isoToDateObject(dtt);
    // tslint:disable-next-line:no-console
    console.log(`Value of receiptDate USED IN  Controller: ${date}`);
    return this.receiptsService.findByDate(date);
  }
}
