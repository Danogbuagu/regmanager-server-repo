import { Test, TestingModule } from '@nestjs/testing';
import { PrtnumbersController } from './prtnumbers.controller';

describe('Prtnumbers Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [PrtnumbersController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: PrtnumbersController = module.get<PrtnumbersController>(PrtnumbersController);
    expect(controller).toBeDefined();
  });
});
