import {
  Controller,
  ValidationPipe,
  Post,
  Body,
  UsePipes,
  Get,
  Param,
  Put,
  Delete,
} from '@nestjs/common';
import { PrtnumbersService } from './prtnumbers.service';
import { CreatePrtnumberDto } from './create-prtnumber.dto';
import { Prtnumber } from './prtnumber.interface';

@Controller('prtnumbers')
export class PrtnumbersController {
  constructor(private readonly prtNbrsService: PrtnumbersService) {}

  @Post()
  // @Roles('admin')
  @UsePipes(new ValidationPipe())
  create(@Body() createPrtnumberDto: CreatePrtnumberDto) {
    this.prtNbrsService.create(createPrtnumberDto);
  }
  /*
curl -X POST -H 'content-type: application/json' -d '{
    "id": 1000,
    "username": "johnbull",
    "sname": "Bull",
    "fname": "John",
    "onames": "Shit",
    "phoneNo": "00055556666",
    "role": "user",
    "active":  true,
    "visits": 0
  }' localhost:3000/users

*/
  // @Get()
  // findAll(@Query() query) {
  //   return `This action returns all users (limit: ${query.limit} items)`;
  // }

  @Get()
  async findAll(): Promise<Prtnumber[]> {
    return this.prtNbrsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id) {
    return this.prtNbrsService.findOne(id); // `This action returns a #${id} user`;
  }

  @Put(':id')
  update(@Param('id') id, @Body() updateprtNbsDto) {
    return this.prtNbrsService.update(id); // `This action updates a #${id} user`;
  }

  @Delete(':id')
  remove(@Param('id') id) {
    return this.prtNbrsService.deleteOne(id); // `This action removes a #${id} user`;
  }
}
