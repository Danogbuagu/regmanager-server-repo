import { Module } from '@nestjs/common';
import { PrtnumbersService } from './prtnumbers.service';
import { PrtnumbersController } from './prtnumbers.controller';
import { PrtnumberSchema } from './prtnumber.schema';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Prtnumber', schema: PrtnumberSchema }])],
  providers: [PrtnumbersService],
  controllers: [PrtnumbersController]
})
export class PrtnumbersModule {}
