import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreatePrtnumberDto } from './create-prtnumber.dto';
import { Prtnumber } from './prtnumber.interface';

@Injectable()
export class PrtnumbersService {
  // private readonly users: Prtnumber[] = [];

  constructor(
    @InjectModel('Prtnumber') private readonly prtNbrModel: Model<Prtnumber>,
  ) {}

  private handleError(error: Error): string {
    return `A database error occured with the following result: ${
      error.message
    }`;
  }

  async create(prtNbrDto: CreatePrtnumberDto): Promise<Prtnumber> {
    const newPrtnumber = new this.prtNbrModel(prtNbrDto);
    return await newPrtnumber.save();
  }

  async findAll(): Promise<Prtnumber[]> {
    return await this.prtNbrModel
      .find()
      .sort('queno')
      .exec();
  }

  async findOne(qno: number): Promise<Prtnumber> {
    return await this.prtNbrModel.find({ queno: qno }).exec();
  }

  async update(id: string): Promise<Prtnumber> {
    return await this.prtNbrModel.update({ id }).exec();
  }

  async deleteOne(id: string): Promise<Prtnumber> {
    return await this.prtNbrModel
      .deleteOne({ queno: parseInt(id, 10) }, err => {
        if (err) return this.handleError(err);
        // deleted at most one queueno document
      })
      .exec();
  }
}
