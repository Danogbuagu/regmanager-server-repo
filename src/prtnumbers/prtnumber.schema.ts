import * as mongoose from 'mongoose';
import { autoIncrement } from 'mongoose-plugin-autoinc';

export const PrtnumberSchema = new mongoose.Schema({
  queno: { type: Number, required: true },
});

PrtnumberSchema.plugin(autoIncrement, {
  model: 'Prtnumber',
  field: 'prtnumberId',
  startAt: 1001,
  incrementBy: 1,
});
