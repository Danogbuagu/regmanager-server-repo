import { IsString, IsInt, IsDate, IsNumber, IsBoolean } from 'class-validator';

export class CreatePrtnumberDto {

  @IsNumber()
  readonly queno: number;
}
