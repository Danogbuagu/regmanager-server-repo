import { Test, TestingModule } from '@nestjs/testing';
import { PrtnumbersService } from './prtnumbers.service';

describe('PrtnumbersService', () => {
  let service: PrtnumbersService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PrtnumbersService],
    }).compile();
    service = module.get<PrtnumbersService>(PrtnumbersService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
