import { Module } from '@nestjs/common';
import { QuenumbersController } from './quenumbers.controller';
import { QuenumbersService } from './quenumbers.service';
import { MongooseModule } from '@nestjs/mongoose';
import { QuenumberSchema } from './quenumber.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Quenumber', schema: QuenumberSchema }]),
  ],
  controllers: [QuenumbersController],
  providers: [QuenumbersService],
})
export class QuenumbersModule {}
