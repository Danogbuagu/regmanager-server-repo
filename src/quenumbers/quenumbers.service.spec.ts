import { Test, TestingModule } from '@nestjs/testing';
import { QuenumbersService } from './quenumbers.service';

describe('QuenumbersService', () => {
  let service: QuenumbersService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [QuenumbersService],
    }).compile();
    service = module.get<QuenumbersService>(QuenumbersService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
