import { CreateQuenumberDto } from './create-quenumber.dto';
import { QuenumbersService } from './quenumbers.service';
import { Quenumber } from './quenumber.interface';
import {
  Controller,
  ValidationPipe,
  Post,
  Body,
  UsePipes,
  Get,
  Param,
  Put,
  Delete,
} from '@nestjs/common';

@Controller('quenumbers')
export class QuenumbersController {  constructor(private readonly queNbrsService: QuenumbersService) {}

@Post()
// @Roles('admin')
@UsePipes(new ValidationPipe())
create(@Body() createQuenumberDto: CreateQuenumberDto) {
  this.queNbrsService.create(createQuenumberDto);
}
/*
curl -X POST -H 'content-type: application/json' -d '{
  "id": 1000,
  "username": "johnbull",
  "sname": "Bull",
  "fname": "John",
  "onames": "Shit",
  "phoneNo": "00055556666",
  "role": "user",
  "active":  true,
  "visits": 0
}' localhost:3000/users

*/
// @Get()
// findAll(@Query() query) {
//   return `This action returns all users (limit: ${query.limit} items)`;
// }

@Get()
async findAll(): Promise<Quenumber[]> {
  return this.queNbrsService.findAll();
}

@Get(':id')
findOne(@Param('id') id) {
  return this.queNbrsService.findOne(id); // `This action returns a #${id} user`;
}

@Put(':id')
update(@Param('id') id, @Body() updatequeNbsDto) {
  return this.queNbrsService.update(id); // `This action updates a #${id} user`;
}

@Delete(':id')
remove(@Param('id') id) {
  return this.queNbrsService.deleteOne(id); // `This action removes a #${id} user`;
}}
