import { Test, TestingModule } from '@nestjs/testing';
import { QuenumbersController } from './quenumbers.controller';

describe('Quenumbers Controller', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [QuenumbersController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: QuenumbersController = module.get<QuenumbersController>(QuenumbersController);
    expect(controller).toBeDefined();
  });
});
