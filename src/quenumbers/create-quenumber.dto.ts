import * as mongoose from 'mongoose';
import { IsInt, IsNumber } from 'class-validator';

export class CreateQuenumberDto {
  @IsInt()
  readonly queno: number;
}
