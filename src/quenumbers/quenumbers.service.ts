import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Quenumber } from './quenumber.interface';
import { CreateQuenumberDto } from './create-quenumber.dto';

@Injectable()
export class QuenumbersService {
  // private readonly users: Quenumber[] = [];

  constructor(
    @InjectModel('Quenumber') private readonly queNbrModel: Model<Quenumber>,
  ) {}

  private handleError(error: Error): string {
    return `A database error occured with the following result: ${
      error.message
    }`;
  }

  async create(prtNbrDto: CreateQuenumberDto): Promise<Quenumber> {
    const newQuenumber = new this.queNbrModel(prtNbrDto);
    return await newQuenumber.save();
  }

  async findAll(): Promise<Quenumber[]> {
    return await this.queNbrModel.find().sort('queno').exec();
  }

  async findOne(id: string): Promise<Quenumber> {
    return await this.queNbrModel.find
      .where('id')
      .equals(id)
      .exec();
  }

  async update(id: string): Promise<Quenumber> {
    return await this.queNbrModel.update({ id }).exec();
  }

  async deleteOne(id: string): Promise<Quenumber> {
    return await this.queNbrModel
      .deleteOne({ queno: parseInt(id, 10) }, err => {
        if (err) return this.handleError(err);
        // deleted at most one queueno document
      })
      .exec();
  }
}
