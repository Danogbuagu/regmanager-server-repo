import * as mongoose from 'mongoose';
import { autoIncrement } from 'mongoose-plugin-autoinc';

export const QuenumberSchema = new mongoose.Schema({
  queno: { type: Number, required: true },
});

QuenumberSchema.plugin(autoIncrement, {
  model: 'Quenumber',
  field: 'queueId',
  startAt: 1001,
  incrementBy: 1,
});
